public void alert(
	String title,
	String message,
	boolean cancelable,
	@Nullable String positiveText,
	@Nullable DialogInterface.OnClickListener onPositiveClick,
	@Nullable String neutralText,
	@Nullable DialogInterface.OnClickListener onNeutralClick,
	@Nullable String negativeText,
	@Nullable DialogInterface.OnClickListener onNegativeClick) {
	// ...
}