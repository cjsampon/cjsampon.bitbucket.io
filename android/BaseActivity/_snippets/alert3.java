public void alert(
	String title,
	String message,
	boolean cancelable,
	String positiveText,
	@Nullable DialogInterface.OnClickListener onPositiveClick,
	@Nullable String negativeText,
	@Nullable DialogInterface.OnClickListener onNegativeClick) {
	// ...
}